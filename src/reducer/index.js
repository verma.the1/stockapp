const rootReducer = (state={},action)=>{
	switch (action.type){
		case 'ADD_STOCKS_DETAILS' : {
				for(var key in action.stocks){
					if([key] in state){
						if(action.stocks[key].price < state[key].price){
							action.stocks[key].color = "red";
						}
						else{
							action.stocks[key].color = "green";
						}
					}
				}
			return { ...state , ...action.stocks }
		}
		default:{
			return state;
		}
	}
}

export default rootReducer;