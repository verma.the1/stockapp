import React from 'react';
import './App.css';
import { connect } from 'react-redux';
import List  from './components/list';

const App = ({stocksDetails}) =>{
    return (
      <div className="App">
        <header className="App-header">
          <table>
            <tbody>
              <tr>
                <th>Ticker</th>
                <th>Price</th>
                <th>Last Update</th>
              </tr>
                {
                  Object.keys(stocksDetails).map((stock,index)=>{
                    return <List key={index} {...stocksDetails[stock]} />
                  })
               }  
            </tbody> 
          </table>
        </header>
      </div>
    );
}

const mapStateToProps = (state)=>{
  return {
    stocksDetails : state
  }
}

export default connect(mapStateToProps)(App)
