import { getStock } from '../action';

export const setupWebsocket = ({ host, port }) =>
  new Promise((resolve) => {
    const webSocket = new WebSocket(`ws://${host}`);

    const receive = (onMessage) => {
      webSocket.onmessage = (event) => onMessage(getStock(JSON.parse(event.data)));
    };

    const send = (type, payload) =>
      webSocket.send(JSON.stringify({ type, payload }));

    webSocket.onopen = () => resolve({ send, receive });
  });