const getTodayDate =()=>{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	var hh = today.getHours();
	var min = today.getMinutes();
	var sec = today.getSeconds();

	if(dd<10) {
		dd = '0'+dd
	} 

	if(mm<10) {
		mm = '0'+mm
	} 

	if(hh<10){
		hh = '0'+hh
	}

	var time = hh + ":" + min + ":" + sec;
	today = `${mm}/${dd}/${yyyy} ${time}`;
	return today;
}
module.exports = {
	getTodayDate
}
