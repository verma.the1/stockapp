import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { setupWebsocket } from './websocket';
import rootReducer from './reducer';

const URL = "stocks.mnet.website";

const setupStore = (host,port) => {
  return setupWebsocket({ host, port }).then(({ send, receive }) => {
    const store = createStore(rootReducer);
    receive(store.dispatch);
    return store;
  });
};

setupStore(URL).then((store) =>{
	ReactDOM.render(
	<Provider store={store} >
		<App/>
	</Provider>, document.getElementById('root'));
serviceWorker.unregister();
});