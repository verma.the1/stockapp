import React from 'react';

const List = ({name,price,color,date})=>{
	return(
		<tr className={color}>
			<td>
				{ name }
			</td>
			<td>
				{ price }
			</td>
			<td>
				{ date }
			</td>
		</tr>
	)
}

export default List;